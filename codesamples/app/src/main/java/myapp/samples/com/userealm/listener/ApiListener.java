package myapp.samples.com.userealm.listener;

public interface ApiListener<T> {
    void onSuccess(T data);
    void onError(Throwable t);
}
