package myapp.samples.com.userealm.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import myapp.samples.com.userealm.realm.RealmDataItem;

public class DataItem implements Serializable {

    @PrimaryKey
    @SerializedName("id")
    public long id;
    @Index
    @SerializedName("name")
    public String name;
    @SerializedName("latitude")
    public double latitude;
    @SerializedName("longitude")
    public double longitude;

    public DataItem() {}

    public DataItem(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public RealmDataItem convertToRealmDataItem() {
        RealmDataItem dataItem = new RealmDataItem();
        dataItem.id = id;
        dataItem.name = name;
        dataItem.latitude = latitude;
        dataItem.longitude = longitude;
        return dataItem;
    }

    @Override
    public int hashCode() {
        int valueHash = 1;
        if (name != null) {
            valueHash ^= name.hashCode();
        }
        return (int) (id ^ valueHash);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        boolean result = false;
        DataItem item = (DataItem) o;
        if (name != null || id >= 0) {
            result = true;
        }
        if (name != null) {
            result &= name.equals(item.name);
        }
        if (id >= 0) {
            result &= id == item.getId();
        }
        return result;
    }
}
