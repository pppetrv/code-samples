package myapp.samples.com.usekotlin.extensions

import android.content.Context

import android.widget.Toast

fun Context.showToast(text: String, length: Int = Toast.LENGTH_LONG) {
    Toast.makeText(this, text, length).show()
}
