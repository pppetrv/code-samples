package myapp.samples.com.userealm.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;

import java.lang.reflect.Type;

import myapp.samples.com.userealm.MyApp;
import myapp.samples.com.userealm.R;

public class AppPrefs {

    private static volatile AppPrefs instance;

    private final Context context;
    private Gson gson;

    private static String KEY_SESSION_ID = "KEY_SESSION_ID";
    private static String KEY_DB_VERSION = "KEY_DB_VERSION";

    private AppPrefs(Context context) {
        this.context = context;
    }

    public static AppPrefs getInstance() {
        return AppPrefs.getInstance(MyApp.getInstance());
    }

    public static AppPrefs getInstance(Context context) {
        AppPrefs localInstance = instance;
        if (localInstance == null) {
            synchronized (AppPrefs.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new AppPrefs(context);
                }
            }
        }
        return localInstance;
    }

    public Gson getGson() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }

    public long getDbVersion() {
        return getLongValue(KEY_DB_VERSION, -1);
    }

    public void setDbVersion(long value) {
        setLongValue(KEY_DB_VERSION, value);
    }

    public String getSessionId() {
        return getStringValue(KEY_SESSION_ID);
    }

    public void setSessionId(String sessionId) {
        setStringValue(KEY_SESSION_ID, sessionId);
    }

    public void storeObject(String key, Object data) {
        String dataStr = "";
        if (data != null) {
            dataStr = getGson().toJson(data);
        }
        setStringValue(key, dataStr);
    }

    public Object getObject(String key, Type type) {
        final String data = getStringValue(key, "");
        if (TextUtils.isEmpty(data)) {
            return null;
        }
        return getGson().fromJson(data.trim(), type);
    }

    public boolean objectAvailable(String key) {
        final String data = getStringValue(key, "");
        if (!TextUtils.isEmpty(data)) {
            return true;
        }
        return false;
    }

    public float getFloatValue(String key, float defaultValue) {
        SharedPreferences preferences = getSharedPreferences(context);
        return preferences.getFloat(key, defaultValue);
    }

    public void setFloatValue(String key, float value) {
        SharedPreferences preferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public long getLongValue(String key, long defaultValue) {
        SharedPreferences preferences = getSharedPreferences(context);
        return preferences.getLong(key, defaultValue);
    }

    public void setLongValue(String key, long value) {
        SharedPreferences preferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public void setIntValue(String key, int value) {
        SharedPreferences preferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public int getIntValue(String key, int defaultValue) {
        SharedPreferences preferences = getSharedPreferences(context);
        return preferences.getInt(key, defaultValue);
    }

    public void setStringValue(String key, String value) {
        SharedPreferences preferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getStringValue(String key) {
        SharedPreferences preferences = getSharedPreferences(context);
        return preferences.getString(key, null);
    }

    public String getStringValue(String key, String defValue) {
        SharedPreferences preferences = getSharedPreferences(context);
        return preferences.getString(key, defValue);
    }

    public boolean getBooleanValue(String key, boolean defaultValue) {
        SharedPreferences preferences = getSharedPreferences(context);
        return preferences.getBoolean(key, defaultValue);
    }

    public void setBooleanValue(String key, boolean value) {
        SharedPreferences preferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    private SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(context.getString(R.string.preferences_name), Context.MODE_PRIVATE);
    }
}
