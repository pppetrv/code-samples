package myapp.samples.com.userealm.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataItems {

    @SerializedName("pageNumber")
    private long pageNumber;
    @SerializedName("pageSize")
    private long pageSize;
    @SerializedName("hasNext")
    private boolean hasNext;
    @SerializedName("version")
    private long version;
    @SerializedName("lines")
    private List<DataItem> items;

    public long getVersion() {
        return version;
    }

    public List<DataItem> getItems() {
        return items;
    }

    public long getPageNumber() {
        return pageNumber;
    }

    public long getPageSize() {
        return pageSize;
    }

    public boolean hasNext() {
        return hasNext;
    }
}
