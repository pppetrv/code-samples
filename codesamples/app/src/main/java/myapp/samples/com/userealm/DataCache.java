package myapp.samples.com.userealm;

import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import io.realm.Case;
import io.realm.DynamicRealm;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmMigration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import myapp.samples.com.userealm.dto.DataItem;
import myapp.samples.com.userealm.dto.DataItems;
import myapp.samples.com.userealm.listener.ApiListener;
import myapp.samples.com.userealm.net.AppNetwork;
import myapp.samples.com.userealm.realm.RealmDataItem;
import myapp.samples.com.userealm.utils.ExecutorServiceManager;

public class DataCache {

    private static volatile DataCache instance;

    private ExecutorServiceManager executor;

    private static final String TAG = DataCache.class.getSimpleName();

    public static final long REALM_SCHEMA_VERSION = 1;

    private volatile boolean dBInitInProgress = false;

    public static DataCache getInstance() {
        DataCache localInstance = instance;
        if (localInstance == null) {
            synchronized (DataCache.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new DataCache();
                }
            }
        }
        return localInstance;
    }

    public DataCache() {
        executor = new ExecutorServiceManager();
    }

    public void saveDataItems(final List<DataItem> items, final ApiListener<Integer> callback) {
        if (executor != null) {
            executor.runOperation(new Runnable() {
                @Override
                public void run() {
                    int count = saveDataItemsSync(items);
                    if (callback != null) {
                        callback.onSuccess(count);
                    } else {
                        Log.i(TAG, "saveDataItems, callback == null");
                    }
                }
            });
        } else {
            dBInitInProgress = false;
        }
    }

    public void refreshDataDb(final ApiListener<Integer> callback) {
        dBInitInProgress = true;
        AppNetwork.getInstance().getDataItemsDb(new ApiListener<DataItems>() {
            @Override
            public void onSuccess(DataItems data) {
                if (data != null) {
                    if (data.getItems() != null) {
                        saveDataItems(data.getItems(), callback);
                    }
                } else {
                    dBInitInProgress = false;
                }
            }
            @Override
            public void onError(Throwable t) {
                dBInitInProgress = false;
                Log.e(TAG, "refreshDataDb", t);
                if (callback != null) {
                    callback.onError(t);
                }
            }
        });
    }

    private int saveDataItemsSync(List<DataItem> items) {
        Realm realm = null;
        int count = 0;
        try {
            RealmConfiguration config = getRealmConfig();
            realm = Realm.getInstance(config);
            RealmDataItem item = null;
            List<RealmDataItem> realmItems = new RealmList<RealmDataItem>();
            realm.beginTransaction();
            for (DataItem i : items) {
                item = i.convertToRealmDataItem();
                realmItems.add(item);
                count++;
            }
            realm.copyToRealmOrUpdate(realmItems);
            realm.commitTransaction();
        } catch (Exception e) {
            Log.d(TAG, "saveDataItemImpl", e);
        }
        if (realm != null) {
            realm.close();
        }
        dBInitInProgress = false;
        return count;
    }

    public boolean isdBInitInProgress() {
        return dBInitInProgress;
    }

    /**
     * Update data to realm database for single data item
     * @param item - item to update data from
     * @param callback - callback for return operation result
     */
    public void updateData(final DataItem item, final ApiListener<Boolean> callback) {
        executor.runOperation(new Runnable() {
            @Override
            public void run() {
                boolean result = updateDataSync(item);
                if (callback != null) {
                    callback.onSuccess(result);
                }
            }
        });
    }

    public boolean updateDataSync(DataItem item) {
        Realm realm = null;
        boolean succeed = false;
        try {
            RealmConfiguration config = getRealmConfig();
            realm = Realm.getInstance(config);
            realm.beginTransaction();
            RealmDataItem realmItem = null;
            realmItem = item.convertToRealmDataItem();
            realm.insertOrUpdate(realmItem);
            realm.commitTransaction();
            succeed = true;
        } catch (Exception e) {
            Log.d(TAG, "updateDataImpl", e);
        }
        if (realm != null) {
            realm.close();
        }
        return succeed;
    }

    /**
     * Search data in database by name, that is in queryStr
     * @param queryStr - query string contains in name field to search by name
     * @param callback - callback for return operation result
     */
    public void findDataItem(final String queryStr, final ApiListener<List<DataItem>> callback) {
        executor.runOperation(new Runnable() {
            @Override
            public void run() {
                List<DataItem> result = findDataItemSync(queryStr);
                if (callback != null) {
                    callback.onSuccess(result);
                }
            }
        });
    }

    public List<DataItem> findDataItemSync(String queryStr) {
        List<DataItem> result = new ArrayList<DataItem>();
        Realm realm = null;
        try {
            RealmConfiguration config = getRealmConfig();
            realm = Realm.getInstance(config);
            RealmQuery<RealmDataItem> query = realm.where(RealmDataItem.class);
            if (!TextUtils.isEmpty(queryStr)) {
                query = query.contains("name", queryStr, Case.INSENSITIVE);
            }
            RealmResults<RealmDataItem> realmResults = query.findAll();
            DataItem item = null;
            for (RealmDataItem i : realmResults) {
                item = i.convertToDataItem();
                result.add(item);
            }
        } catch (Exception e) {
            Log.e(TAG, "findDataItemImpl", e);
        }
        Log.d(TAG, "findDataItemImpl, found items: " + String.valueOf(result.size()));
        if (realm != null) {
            realm.close();
        }
        return result;
    }

    public RealmMigration getRealmMigration() {
        RealmMigration m = new RealmMigration() {
            @Override
            public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {}
        };
        return m;
    }

    public RealmConfiguration getRealmConfig() {
        RealmConfiguration realmConfig =
                new RealmConfiguration.Builder().
                        schemaVersion(REALM_SCHEMA_VERSION).
                        deleteRealmIfMigrationNeeded().
                        build();
        return realmConfig;
    }

    public void clearDataDb() {
        Realm realm = null;
        try {
            RealmConfiguration config = getRealmConfig();
            realm = Realm.getInstance(config);
            realm.beginTransaction();
            realm.delete(RealmDataItem.class);
            realm.commitTransaction();
        } catch (Exception e) {
            Log.d(TAG, "clearDataDb", e);
        }
        if (realm != null) {
            realm.close();
        }
    }
}
