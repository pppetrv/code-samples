package myapp.samples.com.userealm.net;

import myapp.samples.com.userealm.dto.DataItems;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface ApiService {

    @GET("/api/items")
    @Headers({"Content-Type:application/json"})
    Call<DataItems> getDataItemsList(@Header("Cookie") String jSessionId, @Query("pageNumber") Long pageNumber, @Query("pageSize") Long pageSize);

    @GET("/api/items/db")
    @Headers({"Content-Type:application/json"})
    Call<DataItems> getDataItemsDb(@Header("Cookie") String jSessionId, @Query("version") String version);
}
