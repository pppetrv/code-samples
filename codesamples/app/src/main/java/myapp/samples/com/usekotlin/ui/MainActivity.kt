package myapp.samples.com.usekotlin.ui

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import myapp.samples.com.usekotlin.extensions.showToast
import myapp.samples.com.usekotlin.utils.ActivityUtil
import myapp.samples.com.usekotlin.utils.ScheduleTimer

class MainActivity : Activity() {

    private val REPEAT_TIME = 10000L

    var timer: ScheduleTimer = ScheduleTimer(REPEAT_TIME)
    var counter = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showToast("onCreate: started", Toast.LENGTH_LONG)
        timer.setOnTimerTaskListener(object: ScheduleTimer.TimerTaskListener {
            override fun onTimer() {
                counter++
                ActivityUtil.runOnMainThread(Runnable {
                    showToast("onTimer, counter: $counter", Toast.LENGTH_SHORT)
                })
            }
        })
        timer.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        timer.stop()
    }
}
