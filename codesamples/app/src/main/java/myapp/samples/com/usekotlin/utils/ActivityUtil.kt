package myapp.samples.com.usekotlin.utils

import android.content.Intent
import android.os.Looper
import android.os.Handler
import android.os.Bundle

class ActivityUtil {

    companion object {

        private fun scheduleOnMainThread(r: Runnable) {
            Handler(Looper.getMainLooper()).post(r)
        }

        fun scheduleOnMainThread(r: Runnable, delay: Long) {
            Handler(Looper.getMainLooper()).postDelayed(r, delay)
        }

        fun runOnMainThread(r: Runnable) {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                r.run()
            } else {
                scheduleOnMainThread(r)
            }
        }

        fun dumpIntent(i: Intent?): String {
            if (i == null) {
                return ""
            }
            return dumpBundle(i.extras)
        }

        fun dumpBundle(bundle: Bundle?): String {
            if (bundle == null) {
                return ""
            }
            val s = StringBuilder()
            if (bundle != null) {
                val keys = bundle.keySet()
                val it = keys.iterator()
                while (it.hasNext()) {
                    val key = it.next()
                    s.append("[" + key + "= " + bundle.get(key) + "]").append("\n")
                }
            }
            return s.toString()
        }
    }
}