package myapp.samples.com.userealm.utils;

import android.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ExecutorServiceManager {

    private static final int AWAIT_TERMINATION_TIME = 10;
    private static String TAG = ExecutorServiceManager.class.getSimpleName();
    private ExecutorService executorService;
    private volatile Future currentFuture;

    private boolean isExecutorActive() {
        return executorService != null && !executorService.isShutdown() && !executorService.isTerminated();
    }

    public void runOperation(Runnable run) {
        runOperation(run, false);
    }

    public void runOperation(final Runnable run, boolean cancelPrev) {
        if (executorService == null) {
            executorService = Executors.newSingleThreadExecutor();
        }
        if (cancelPrev && currentFuture != null && !currentFuture.isDone()) {
            currentFuture.cancel(true);
        }
        if (isExecutorActive()) {
            currentFuture = executorService.submit(run);
        }
    }

    public void shutdown() {
        if (!isExecutorActive()) {
            return;
        }
        if (currentFuture != null && !currentFuture.isDone()) {
            currentFuture.cancel(true);
        }
        executorService.shutdown();
        try {
            executorService.awaitTermination(AWAIT_TERMINATION_TIME, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }
}
