package myapp.samples.com.userealm.net;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import myapp.samples.com.userealm.dto.DataItems;
import myapp.samples.com.userealm.listener.ApiListener;
import myapp.samples.com.userealm.utils.AppPrefs;
import myapp.samples.com.userealm.utils.Constants;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppNetwork {

    private static final String TAG = AppNetwork.class.getSimpleName();

    private static AppNetwork clNetwork;
    private Retrofit retrofit;
    private String jSessionId = null;

    public static AppNetwork getInstance() {
        if (clNetwork == null) {
            clNetwork = new AppNetwork();
            String jSessionId = AppPrefs.getInstance().getSessionId();
            clNetwork.setjSessionId(jSessionId);
        }
        return clNetwork;
    }

    public void setjSessionId(String jSessionId) {
        this.jSessionId = jSessionId;
    }

    public String getjSessionId() {
        return jSessionId;
    }

    public void clear() {
        jSessionId = null;
    }

    public void getCollegesList(Long pageNumber, Long pageSize, final ApiListener<DataItems> callback) {
        getApiService().getDataItemsList(jSessionId, pageNumber, pageSize).enqueue(new Callback<DataItems>() {
            @Override
            public void onResponse(Call<DataItems> call, Response<DataItems> response) {
                DataItems data = null;
                if (response != null) {
                    data = response.body();
                }
                if (callback != null) {
                    callback.onSuccess(data);
                }
            }

            @Override
            public void onFailure(Call<DataItems> call, Throwable t) {
                if (callback != null) {
                    callback.onError(t);
                }

            }
        });
    }

    public void getDataItemsDb(final ApiListener<DataItems> callback) {
        long dbVersion = AppPrefs.getInstance().getDbVersion();
        String dbVersionStr = dbVersion >= 0 ? String.valueOf(dbVersion) : null;
        getApiService().getDataItemsDb(jSessionId, dbVersionStr).enqueue(new Callback<DataItems>() {
            @Override
            public void onResponse(Call<DataItems> call, Response<DataItems> response) {
                DataItems data = null;
                if (response != null) {
                    data = response.body();
                }
                if (callback != null) {
                    callback.onSuccess(data);
                }
            }

            @Override
            public void onFailure(Call<DataItems> call, Throwable t) {
                if (callback != null) {
                    callback.onError(t);
                }
            }
        });
    }

    public ApiService getApiService() {
        return getRetrofitInstance(false).create(ApiService.class);
    }

    private Retrofit getRetrofitInstance(final boolean nullable) {
        if (retrofit == null) {
            GsonBuilder builder = new GsonBuilder()
                    .setLenient();
            if (nullable) {
                builder.serializeNulls();
            }
            Gson gson = builder.create();

            OkHttpClient client = getUnsafeOkHttpClient();

            retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.SERVER_ADDRESS)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

    private OkHttpClient getUnsafeOkHttpClient() {
        try {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder = builder.addInterceptor(interceptor);
            builder = builder.hostnameVerifier(new NullHostNameVerifier());
            return builder.build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static class NullHostNameVerifier implements HostnameVerifier {

        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }
}
