# Code Samples #

### What is this repository for? ###

Code samples are located in project Codesamples.
 
Project contains classes in two package names:

### myapp.samples.com.usekotlin ###
Some simle methods that shows use of Kotlin in my projects.

### myapp.samples.com.userealm ###
In this package name are classes and logic that shows implementation of 
caching data objects from server to local database using realm library I used on one of my projects.
